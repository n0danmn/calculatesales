package jp.alhinc.iwahori_arisa.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args){
		
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
		Map <String, String> branchNameMap = new HashMap <>();
		Map <String, Long> totalMap = new HashMap <>();
		BufferedReader br = null;
		
		//args[0]をdirePathにいれる
		String dirPath = args[0];
		
		//readメソッドを呼び出します。その内容がtrueで返ってきてなかったら処理終了にする。
		if(!lstRead(dirPath, "branch.lst", branchNameMap, totalMap,"支店", "^[0-9]{3}$")){
			return;
		}
		
		//集計処理はメインメソッドに
		File dir = new File(dirPath);
		File[] lists = dir.listFiles();
		ArrayList<File> BigList = new ArrayList<>();
		List<Integer> NumberFile = new ArrayList<>();

		
		//for文内listsに入ってる要素の文だけ作業繰り返す
		for(int i =0; i < lists.length; i++){
			//(ファイルかつ8桁.rcd)の条件に合うものを取り出す
			if(lists[i].isFile() && (lists[i].getName()).matches("\\d{8}.rcd")){
			
				//8桁.rcdをBigListに溜め込む
				BigList.add (lists[i]) ;
				
				String codeName = (lists[i].getName());
				
				//売り上げファイル名のうち数字だけ取り出す(前につく0は取られる）
				int onlyNum = Integer.parseInt (codeName.substring(0,8));
				NumberFile.add(onlyNum);
			}
		}

		//連番チェック
		for(int i=1; i < NumberFile.size(); i++){
			if(NumberFile.get(i) - NumberFile.get(i-1) != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//BigFile内のデータ分繰り返す
		for(int i =0; i < BigList.size(); i++){
			try{
				FileReader fr = new FileReader(BigList.get(i));
				br = new BufferedReader(fr);
				
				String line;
				
				//BigListの各ファイルごとの内容を一行ずつだす、出した内容をまとめる箱をrcdBoxとする
				ArrayList<String> rcdBox = new ArrayList<>();
				
				while((line = br.readLine()) != null) {
					//rcdBoxに加えていく、１ファイル終わったらwhileループ終了
					rcdBox.add(line);
					}
				if((rcdBox.size()) != 2){
					System.out.println(lists[i].getName() + "のフォーマットが不正です");
					return;
				}
				//rcdBoxの二行目が数字だけか正規表現で確認
				if(!rcdBox.get(1).matches ("^[0-9]*$")){
						System.out.println("予期せぬエラーが発生しました");
						return;
				}
				//定義ファイル内の支店コード以外のものが売り上げファイルに存在します}
				if(!branchNameMap.containsKey(rcdBox.get(0))){
						System.out.println(lists[i].getName() + "の支店コードが不正です");
						return;
				}
				
				//売上額を入れる箱つくる
				Long before = totalMap.get(rcdBox.get(0));
				Long after = before + Long.parseLong(rcdBox.get(1));
				
				//数値しか入らないafterの箱を文字列がはいる箱に変える
				String str = new Long(after).toString();
				
				//文字の数(=要素数）が10以上か否か
				if(str.length() > 10){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//totalMapのキーに支店コード、値に売上額をいれる
				totalMap.put(rcdBox.get(0), after);
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				} finally {
					if(br != null) {
						try{
							br.close();
							} catch(IOException e) {
								System.out.println("予期せぬエラーが発生しました");
								return;
							}
						}
				}
		}
		
		//outPrintのメソッドを呼び出します。true以外で返ってきたら処理終了
		if(!outPrint(dirPath, "branch.out", branchNameMap, totalMap)){
			return;
		}
	}
	
	/*どちらもbooleanで戻り値を返すメソッド。()内にはこのメソッド内で必要な引数を入力しておく
	 try内の処理でエラーがあったらメインメソッドにfalseで返し、エラーなく処理を終えたらtrueで返す*/
	static boolean lstRead(String dirPath, String fileName, Map <String, String> nameMap, 
			Map <String, Long> totalMap, String defintionFileName, String codeNumber){
		
		BufferedReader br = null;
		try {
			File file = new File(dirPath, fileName);
			if(!file.exists()){
				System.out.println(defintionFileName + "定義ファイルが存在しません");
				return false;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			String line;
			
			//branch.lstの内容を一行ずつだしますよ
			while((line = br.readLine()) != null) {
				//取り出した一行を","を境目に分割しますよ　splitを使って
				String[] items = line.split(",");
				
				//ひとつのファイルから取り出した行は何行か(=要素数)
				int how = items.length;
				if(how != 2){
					System.out.println(defintionFileName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				
				//分割した内容のうちitems[0]に振り分けられた支店コードは数字三桁だけになってますか
				if(!items[0].matches (codeNumber)){
					System.out.println(defintionFileName + "定義ファイルのフォーマットが不正です");
					return false;
					}
				//分割したものをそれぞれマップに振り分けて、値を出す
				nameMap.put(items[0], items[1]);
				totalMap.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try{
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	
	static boolean outPrint(String dirPath, String fileName, 
			Map <String, String> nameMap, Map <String, Long> totalMap){

	//これまでの内容を外部ファイルへ出力
	BufferedWriter bw =null;
	try{
		File branchOutFile = new File(dirPath,fileName);
		FileWriter fw = new FileWriter(branchOutFile);
		bw = new BufferedWriter(fw);
		
		
		for(Map.Entry<String, String> entry : nameMap.entrySet()){
			bw.write(entry.getKey() + ", "+ entry.getValue() + ", "+ totalMap.get(entry.getKey()));
			bw.newLine();
		}
		
	} catch(IOException e) {
		System.out.println("予期せぬエラーが発生しました");
		return false;
	} finally {
		
		if(bw != null) {
			try{
				bw.close();
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
	}
	return true;
	}
	
}//classの終了